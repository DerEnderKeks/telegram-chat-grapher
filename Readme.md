# Telegram Chat Grapher ([Try me!](https://derenderkeks.gitlab.io/telegram-chat-grapher/))

This is a small tool that allows you to see fancy graphs for [Telegram](https://telegram.org/) chats.  
It utilises the data export feature of Telegram, is browser based and works completely client site.

## Usage

1. Open the tool (either [here](https://derenderkeks.gitlab.io/telegram-chat-grapher/) or download this repo and open the index.html file)
2. Select the `result.json` file (read [this](https://telegram.org/blog/export-and-more) to learn how to get that file; you only need to export personal chats but make sure to export as JSON)
3. Select the chat you want to see graphs for
4. ???
5. ~~Profit~~ Graphs!

---

All rights reserved (c) 2020 DerEnderKeks
