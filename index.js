let charts = {}
let tgChats = []

// chats = [{name, type, id, messages}]
const displayChatList = () => {
    let listElement = document.getElementById('chats')
    tgChats.forEach(chat => {
        let newElement = document.createElement('option')
        newElement.setAttribute('value', chat.id)
        newElement.innerText = `${chat.name || '[No name]'} <${chat.id}>`
        listElement.appendChild(newElement)
    })
    document.getElementById('chatsContainer').classList.remove('hide')
}

const handleFileSelect = (event) => {
    let file = event.target.files[0]
    const reader = new FileReader()
    reader.onload = (e) => {
        let tgData = JSON.parse(e.target.result.toString())
        let chats = tgData && tgData.chats && tgData.chats.list || null
        tgChats = chats.filter(c => c.type === 'personal_chat')
        if (!tgChats) return
        displayChatList()
        handleSelect()
    }
    reader.readAsText(file)
}

const getParticipants = chat => {
    const noName = '[No name]'
    let participants = {a: {}, b: {}}
    participants.a.id = chat.messages[0].from_id || -1
    participants.a.name = chat.messages[0].from || noName
    let msgOfB = chat.messages.find(m => m.from_id !== participants.a.id) || {}
    participants.b.id = msgOfB.from_id || -1
    participants.b.name = msgOfB.from || noName
    return participants
}

const renderMostUsedWords = chat => {
    let mostUsedWordsData = new Map() // {<word>: {<IDa>: <count>, <IDb>: <count>}
    let participants = getParticipants(chat)

    chat.messages.forEach(m => {
        let text = m.text
        if (Array.isArray(text)) {
            text = ''
            m.text.forEach(t => {
                if (t instanceof Object) t = t.text || ''
                text += t
            })
        }
        let words = text.split(/\s/)
        words.forEach(w => {
            w = w.trim().toLowerCase()
            w = w.replace(/[!?.,;:)\]"'*]+$/, '')
            w = w.replace(/^[("'\[*]+/, '')
            if (w.length === 0) return
            let wordData = mostUsedWordsData.get(w) || {}
            wordData[m.from_id] = (wordData[m.from_id] || 0) + 1
            mostUsedWordsData.set(w, wordData)
        })
    })

    mostUsedWordsData.forEach((v, k) => { // add total word count
        v.total = (v[participants.a.id] || 0) + (v[participants.b.id] || 0)
        mostUsedWordsData.set(k, v)
    })

    // sort desc by total
    mostUsedWordsData = new Map([...mostUsedWordsData].sort((a, b) => (a[1].total < b[1].total && 1) || a[1].total === b[1].total ? 0 : -1))

    let labels = []
    let mostUsedWordsDatasets = [{
        label: participants.a.name,
        data: [],
        backgroundColor: 'rgba(110, 68, 255, 1)'
    }, {
        label: participants.b.name,
        data: [],
        backgroundColor: 'rgba(184, 146, 255, 1)'
    }]

    let maxCount = 0 // used to center the x axis
    for (const [k, v] of mostUsedWordsData) {
        if (labels.length === 30) break // Limit to top n
        if ((v[participants.a.id] || 0) > maxCount) maxCount = v[participants.a.id]
        if ((v[participants.b.id] || 0) > maxCount) maxCount = v[participants.b.id]
        labels.push(k)
        mostUsedWordsDatasets[0].data.push(-v[participants.a.id] || 0)
        mostUsedWordsDatasets[1].data.push(v[participants.b.id] || 0)
    }

    if (charts.mostUsedWordsChart) charts.mostUsedWordsChart.destroy()
    charts.mostUsedWordsChart = new Chart('chartMostUsedWords', {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: mostUsedWordsDatasets
        },
        options: {
            title: {
                display: true,
                text: 'Most used words',
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                }],
                xAxes: [{
                    ticks: {
                        callback: value => {
                            value = Math.abs(value)
                            if (value === maxCount) return '' // hide last tick TODO try to hide gridline too
                            return value // don't display as negative numbers
                        },
                        min: -maxCount,
                        max: maxCount,
                    },
                    position: 'top',
                    gridLines: {
                        drawBorder: false,
                    },
                }],
            },
            legend: {
                position: 'bottom'
            }
        }
    });
}

const renderMessagesPerWeekday = chat => {
    let messagesPerWeekdayData = new Map() // {<weekday>: {<IDa>: <count>, <IDb>: <count>}
    let participants = getParticipants(chat)

    const weekdayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

    chat.messages.forEach(m => {
        let messageWeekday = new Date(m.date).getDay()
        let weekdayData = messagesPerWeekdayData.get(messageWeekday) || {}
        weekdayData[m.from_id] = (weekdayData[m.from_id] || 0) + 1
        messagesPerWeekdayData.set(messageWeekday, weekdayData)
    });

    ([...weekdayNames.keys()]).forEach(w => {
        // making sure every day is present, even when no messages for it exist
        messagesPerWeekdayData.get(w) || messagesPerWeekdayData.set(w, {})
    })

    let messagesPerWeekdayDatasets = [{
        label: participants.a.name,
        data: [],
        borderColor: 'rgba(110, 68, 255, 1)'
    }, {
        label: participants.b.name,
        data: [],
        borderColor: 'rgba(184, 146, 255, 1)'
    }]

    messagesPerWeekdayData = new Map([...messagesPerWeekdayData].sort((a, b) => (a[0] > b[0] && 1) || a[0] === b[0] ? 0 : -1))

    for (const [_, v] of messagesPerWeekdayData) {
        messagesPerWeekdayDatasets[0].data.push(v[participants.a.id] || 0)
        messagesPerWeekdayDatasets[1].data.push(v[participants.b.id] || 0)
    }

    if (charts.messagesPerWeekdayChart) charts.messagesPerWeekdayChart.destroy()
    charts.messagesPerWeekdayChart = new Chart('chartMessagesPerWeekday', {
        type: 'radar',
        data: {
            labels: weekdayNames,
            datasets: messagesPerWeekdayDatasets
        },
        options: {
            title: {
                display: true,
                text: 'Messages per weekday',
            },
            legend: {
                position: 'bottom'
            }
        }
    });
}

const renderMessagesPerHour = chat => {
    let messagesPerHourData = new Map() // {<hour>: {<IDa>: <count>, <IDb>: <count>}
    let participants = getParticipants(chat)

    chat.messages.forEach(m => {
        if (m.type !== 'message') return
        let messageHour = new Date(m.date).getHours().toString()
        let hourData = messagesPerHourData.get(messageHour) || {}
        hourData[m.from_id] = (hourData[m.from_id] || 0) + 1
        messagesPerHourData.set(messageHour, hourData)
    })

    let messagesPerHourDatasets = [{
        label: participants.a.name,
        data: [],
        borderColor: 'rgba(110, 68, 255, 1)'
    }, {
        label: participants.b.name,
        data: [],
        borderColor: 'rgba(184, 146, 255, 1)'
    }]


    let labels = [];
    ([...new Array(24).keys()]).forEach(k => {
        k = k.toString()
        labels.push(k) // create labels
        messagesPerHourData.get(k) || messagesPerHourData.set(k, {}) // make sure hour exists
    })

    messagesPerHourData = new Map([...messagesPerHourData].sort((a, b) => (Number(a[0]) > Number(b[0]) && 1) || Number((a[0]) === Number(b[0]) ? 0 : -1)))

    for (const [k, v] of messagesPerHourData) {
        messagesPerHourDatasets[0].data.push(v[participants.a.id] || 0)
        messagesPerHourDatasets[1].data.push(v[participants.b.id] || 0)
    }

    if (charts.messagesPerHourChart) charts.messagesPerHourChart.destroy()
    charts.messagesPerHourChart = new Chart('chartMessagesPerHour', {
        type: 'radar',
        data: {
            labels: labels,
            datasets: messagesPerHourDatasets,
        },
        options: {
            title: {
                display: true,
                text: 'Messages per hour',
            },
            legend: {
                position: 'bottom'
            },
            scale: {
                ticks: {
                    beginAtZero: true
                }
            },
        }
    });
}


const renderGraphs = chat => {
    renderMostUsedWords(chat)
    renderMessagesPerWeekday(chat)
    renderMessagesPerHour(chat)
}

const handleSelect = () => {
    const selectedChatID = document.getElementById('chats').selectedOptions[0].value
    let chat = tgChats.find(c => c.id.toString() === selectedChatID)
    if (!chat) return
    renderGraphs(chat)
}

window.addEventListener('load', () => {
    // for some reason my sorting algorithm is broken in anything but Chrome
    if (!window.chrome) window.alert('Get a proper Browser. Only Chrome is supported.')

    Chart.defaults.global.defaultFontFamily = "'Open Sans', sans-serif"
    Chart.defaults.global.title.fontSize = 18
    Chart.defaults.global.responsive = false

    document.getElementById('file').addEventListener('change', handleFileSelect, false);
    document.getElementById('chats').addEventListener('change', handleSelect, false);
})
